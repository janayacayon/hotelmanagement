# Hotel Management

This project follows these tutorials:
* [Razor Pages with Entity Framework Core in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/data/ef-rp/intro?view=aspnetcore-3.1&tabs=visual-studio)

## Tools
* VS Code - v1.45.0

### Extensions
* C# - Microsoft
* C# Extensions - jchannon
* SQL Server (mssql) - Microsoft

## Installation
Is comming soon...

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
This is just a learning project.