﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagement.Models
{
    public enum Class
    {
        A, B, C, D, E
    }

    public class Reservation
    {
        public int ID { get; set; }
        public int HotelID { get; set; }
        public int CustomerID { get; set; }
        public Class? Class { get; set; }

        public Customer Customer { get; set; }
        public Hotel Hotel { get; set; }
    }
}
