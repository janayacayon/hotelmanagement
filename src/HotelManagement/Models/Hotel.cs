﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagement.Models
{
    public class Hotel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HotelID { get; set; }
        public string Name { get; set; }
        public int Credits { get; set; }
        public ICollection<Reservation> Reservations { get; set; }
    }
}
