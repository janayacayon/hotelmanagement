using System;
using System.Linq;
using HotelManagement.Models;

namespace HotelManagement.Data
{
    public class DbInitializer
    {
        public static void Initialize(HostelryContext context)
        {
            context.Database.EnsureCreated();

            if (context.Customers.Any())
            {
                return;
            }

            var customers = new Customer[]
            {
                new Customer{LastName="Joller",FirstName="Levin",EnrollmentDate=DateTime.Parse("2020-02-23")},
                new Customer{LastName="Müller",FirstName="Tim",EnrollmentDate=DateTime.Parse("2017-12-23")},
                new Customer{LastName="Birke",FirstName="Jasmin",EnrollmentDate=DateTime.Parse("2014-03-23")},
                new Customer{LastName="Huber",FirstName="Hermine",EnrollmentDate=DateTime.Parse("2003-05-23")},
                new Customer{LastName="Meyer",FirstName="Harry",EnrollmentDate=DateTime.Parse("2019-02-23")},
            };
            foreach (Customer c in customers)
            {
                context.Customers.Add(c);
            }
            context.SaveChanges();

            var hotels = new Hotel[]
            {
                new Hotel{HotelID=1234,Name="Gütsch",Credits=1394},
                new Hotel{HotelID=4231,Name="Rosenegg",Credits=5323},
                new Hotel{HotelID=4334,Name="Sternen",Credits=3412},
                new Hotel{HotelID=1127,Name="Mama",Credits=10},
            };
            foreach (Hotel h in hotels)
            {
                context.Hotels.Add(h);
            }
            context.SaveChanges();

            var reservations = new Reservation[]
            {
                new Reservation{CustomerID=1,HotelID=1234,Class=Class.A},
                new Reservation{CustomerID=1,HotelID=1127,Class=Class.C},
                new Reservation{CustomerID=2,HotelID=1234},
                new Reservation{CustomerID=2,HotelID=4231,Class=Class.A},
                new Reservation{CustomerID=2,HotelID=1127,Class=Class.A},
                new Reservation{CustomerID=3,HotelID=1234,Class=Class.E},
                new Reservation{CustomerID=3,HotelID=1234},
                new Reservation{CustomerID=4,HotelID=4334,Class=Class.C},
                new Reservation{CustomerID=4,HotelID=4231,Class=Class.B},
                new Reservation{CustomerID=5,HotelID=4334,Class=Class.A}
            };
            foreach (Reservation r in reservations)
            {
                context.Reservations.Add(r);
            }
            context.SaveChanges();
        }
    }
}