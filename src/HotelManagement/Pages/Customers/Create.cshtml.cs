using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using HotelManagement.Models;
using HotelManagement.Data;

namespace HotelManagement.Pages_Customers
{
    public class CreateModel : PageModel
    {
        private readonly HostelryContext _context;

        public CreateModel(HostelryContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Customer Customer { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            var emptyCustomer = new Customer();

            // Protect from overposting attacks
            if (await TryUpdateModelAsync<Customer>(
                emptyCustomer,
                "customer",
                c => c.FirstName, c => c.LastName, c => c.EnrollmentDate))
            {
                await _context.Customers.AddAsync(emptyCustomer);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
