using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HotelManagement.Models;
using HotelManagement.Data;

namespace HotelManagement.Pages_Customers
{
    public class EditModel : PageModel
    {
        private readonly HostelryContext _context;

        public EditModel(HostelryContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Customer Customer { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Customer = await _context.Customers
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.ID == id);

            if (Customer == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var customerToUpdate = _context.Customers.FindAsync(id);

            if (customerToUpdate == null)
            {
                return NotFound();
            }

            // Protect from overposting attacks
            if (await TryUpdateModelAsync<Customer>(
                //Must be await, because of FindAsync()
                await customerToUpdate,
                "customer",
                c=>c.LastName, c=>c.FirstName, c=>c.EnrollmentDate))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
